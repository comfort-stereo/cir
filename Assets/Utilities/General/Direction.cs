﻿using System;
using System.Linq;

namespace Utilities.General {

    public enum Direction : byte {
        None, 
        N, 
        S, 
        E, 
        W, 
        NW, 
        NE, 
        SW, 
        SE
    }
    
    public static class DirectionExtensions {
        
        public static Direction[] All { get; } = 
            ((Direction[]) Enum.GetValues(typeof(Direction)))
            .Where(direction => direction != Direction.None).ToArray();

        public static bool IsSquare(this Direction direction) {
            var value = (int) direction;
            return value > 0 && value < 5;
        }

        public static bool IsDiagonal(this Direction direction) {
            return !IsSquare(direction);
        }
        
        public static Direction Opposite(this Direction direction) {
            switch (direction) {
                case Direction.N: return Direction.S;
                case Direction.S: return Direction.N;
                case Direction.E: return Direction.W;
                case Direction.W: return Direction.E;
                case Direction.NW: return Direction.SE;
                case Direction.NE: return Direction.SW;
                case Direction.SW: return Direction.NE;
                case Direction.SE: return Direction.NW;
                case Direction.None: return Direction.None; 
                default: throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public static IntVector2 ToIntVector(this Direction direction) {
            switch (direction) {
                case Direction.N: return IntVector2.N; 
                case Direction.S: return IntVector2.S;
                case Direction.E: return IntVector2.E; 
                case Direction.W: return IntVector2.W; 
                case Direction.NW: return new IntVector2(-1, 1);
                case Direction.NE: return new IntVector2(1, 1);
                case Direction.SW: return new IntVector2(-1, -1);
                case Direction.SE: return new IntVector2(1, -1);
                case Direction.None: return IntVector2.Zero;
                default: throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
    }
}