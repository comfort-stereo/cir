﻿using UnityEngine;

namespace Utilities.General {
    
    public static class VectorUtility {
        public static Vector3 Zero => new Vector3(0, 0, 0);
        public static Vector3 Up => new Vector3(0, 1, 0);
        public static Vector3 Down => new Vector3(0, -1, 0);
        public static Vector3 Left => new Vector3(-1, 0, 0);
        public static Vector3 Right => new Vector3(1, 0, 0);
        public static Vector3 One => new Vector3(1, 1, 1);
        public static Vector3 Higher => new Vector3(0, 0, 1);
        public static Vector3 Lower => new Vector3(0, 0, -1);
    }
}