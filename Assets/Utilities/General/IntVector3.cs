﻿using System;
using UnityEngine;

namespace Utilities.General {
    
    public struct IntVector3 : IEquatable<IntVector3> {
        public int X;
        public int Y;
        public int Z;
        
        public static IntVector2 Zero => new IntVector3(0, 0, 0);
        public static IntVector2 Up => new IntVector3(0, 1, 0);
        public static IntVector2 Down => new IntVector3(0, -1, 0);
        public static IntVector2 Left => new IntVector3(-1, 0, 0);
        public static IntVector2 Right => new IntVector3(1, 0, 0);
        public static IntVector2 One => new IntVector3(1, 1, 1);
        public static IntVector2 Higher => new IntVector3(0, 0, 1);
        public static IntVector2 Lower => new IntVector3(0, 0, -1);

        public IntVector3(int x, int y, int z) {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public IntVector3(Vector3 vector) : this((int) vector.x, (int) vector.y, (int) vector.z) { }

        public static bool operator ==(IntVector3 a, IntVector3 b) => a.Equals(b);
        public static bool operator !=(IntVector3 a, IntVector3 b) => a.Equals(b);
        public static IntVector3 operator +(IntVector3 a, IntVector3 b) => new IntVector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        public static IntVector3 operator -(IntVector3 a, IntVector3 b) => new IntVector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        public static IntVector3 operator *(IntVector3 vector, int scalar) => new IntVector3(vector.X * scalar, vector.Y * scalar, vector.Z * scalar);
        public static IntVector3 operator /(IntVector3 vector, int scalar) => new IntVector3(vector.X / scalar, vector.Y / scalar, vector.Z / scalar);
    
        public float Magnitude => Mathf.Sqrt(this.SquareMagnitude);
        public float SquareMagnitude => this.X * this.X + this.Y * this.Y + this.Z * this.Z;
    
        public static implicit operator Vector2(IntVector3 vector) => new Vector2(vector.X, vector.Y);
        public static implicit operator Vector3(IntVector3 vector) => new Vector3(vector.X, vector.Y, vector.Z);
        public static implicit operator IntVector2(IntVector3 vector) => new IntVector2(vector.X, vector.Y);

        public override int GetHashCode() {
            unchecked {
                var hash = this.X;
                hash = (hash * 397) ^ this.Y;
                hash = (hash * 397) ^ this.Z;
                return hash;
            }
        }

        public bool Equals(IntVector3 other) {
            return this.X == other.X && this.Y == other.Y && this.Z == other.Z;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            return obj is IntVector3 && Equals((IntVector3) obj);
        }
    }
}