﻿namespace Utilities.General {
    
    public static class Utility {
        public static bool IsNull(object obj) => ReferenceEquals(obj, null);
        public static bool IsNotNull(object obj) => !IsNull(obj);
    }
}