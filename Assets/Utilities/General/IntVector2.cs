﻿using System;
using UnityEngine;

namespace Utilities.General {
    
    public struct IntVector2 : IEquatable<IntVector2> {
        public int X;
        public int Y;

        public static IntVector2 N => new IntVector2(0, 1);
        public static IntVector2 S => new IntVector2(0, -1);
        public static IntVector2 E => new IntVector2(1, 0);
        public static IntVector2 W => new IntVector2(-1, 0);
        
        public static IntVector2 Up => new IntVector2(0, 1);
        public static IntVector2 Down => new IntVector2(0, -1);
        public static IntVector2 Left => new IntVector2(-1, 0);
        public static IntVector2 Right => new IntVector2(1, 0);
        
        public static IntVector2 One => new IntVector2(1, 1);
        public static IntVector2 Zero => new IntVector2(0, 0);

        public IntVector2(int x, int y) {
            this.X = x;
            this.Y = y;
        }

        public IntVector2(Vector2 vector) : this((int) vector.x, (int) vector.y) { }

        public static bool operator ==(IntVector2 a, IntVector2 b) => a.X == b.X && a.Y == b.Y; 
        public static bool operator !=(IntVector2 a, IntVector2 b) => a.X != b.X || a.Y != b.Y; 
        public static IntVector2 operator +(IntVector2 a, IntVector2 b) => new IntVector2(a.X + b.X, a.Y + b.Y);
        public static IntVector2 operator -(IntVector2 a, IntVector2 b) => new IntVector2(a.X - b.X, a.Y - b.Y);
        public static IntVector2 operator *(IntVector2 vector2, int scalar) => new IntVector2(vector2.X * scalar, vector2.Y * scalar);
        public static IntVector2 operator /(IntVector2 vector2, int scalar) => new IntVector2(vector2.X / scalar, vector2.Y / scalar);
    
        public float Magnitude => Mathf.Sqrt(this.SquareMagnitude);
        public float SquareMagnitude => this.X * this.X + this.Y * this.Y;
    
        public static implicit operator Vector2(IntVector2 vector2) => new Vector2(vector2.X, vector2.Y);
        public static implicit operator Vector3(IntVector2 vector2) => new Vector3(vector2.X, vector2.Y, 0f);
        public static implicit operator IntVector3(IntVector2 vector2) => new IntVector3(vector2.X, vector2.Y, 0);

        public override int GetHashCode() {
            unchecked {
                return (this.X * 397) ^ this.Y;
            }
        }

        public bool Equals(IntVector2 other) {
            return this.X == other.X && this.Y == other.Y;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            return obj is IntVector2 && Equals((IntVector2) obj);
        }

        public override string ToString() {
            return $"({this.X}, {this.Y})";
        }
    }
}
