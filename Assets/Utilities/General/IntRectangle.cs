﻿using System;

namespace Utilities.General {
    
    public struct IntRectangle : IEquatable<IntRectangle> {
        public IntVector2 Start;
        public IntVector2 End;
        
        public static IntRectangle Zero => new IntRectangle(0, 0, 0, 0);

        public IntRectangle(IntVector2 start, IntVector2 end) {
            this.Start = start;
            this.End = end;
        }

        public IntRectangle(int xStart, int yStart, int xEnd, int yEnd) {
            this.Start = new IntVector2(xStart, yStart);
            this.End = new IntVector2(xEnd, yEnd);
        }
        
        public bool Contains(IntVector2 position) {
            return position.X >= this.Start.X && position.X <= this.End.X && 
                   position.Y >= this.Start.Y && position.Y <= this.End.Y;
        }

        public bool Equals(IntRectangle other) => this.Start.Equals(other.Start) && this.End.Equals(other.End);

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            return obj is IntRectangle && Equals((IntRectangle) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (this.Start.GetHashCode() * 397) ^ this.End.GetHashCode();
            }
        }

        public static bool operator ==(IntRectangle a, IntRectangle b) => a.Equals(b);
        public static bool operator !=(IntRectangle a, IntRectangle b) => !a.Equals(b);

        public override string ToString() => $"Start: {this.Start}, End: {this.End}";
    }
}