﻿using System;

namespace Utilities.General {
    
    public struct Optional<T> {
        readonly T value;
        
        public bool IsSome { get; }
        public bool IsNone => !this.IsSome; 

        public T Value {
            get {
                if (this.IsNone) {
                    throw new InvalidOperationException();
                }
                return this.value;
            }
        }
        
        public static Optional<T> None => new Optional<T>();
        public static Optional<T> Some(T value) => new Optional<T>(value);

        Optional(T value) {
            this.value = value;
            this.IsSome = true;
        }
    }
}