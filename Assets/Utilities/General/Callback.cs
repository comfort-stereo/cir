﻿namespace Utilities.General {
    
    public delegate void Callback();
    public delegate void ArgumentCallback<in T>(T argument);
}