﻿using UnityEngine;

namespace Utilities.Extensions {
    
    public static class ObjectExtensions {
        
        public static Transform CreateTransform(string name) {
            return new GameObject(name).transform;
        }

        public static Transform CreateTransform(string name, Transform parent) {
            var transform = CreateTransform(name);
            transform.parent = parent;
            return transform;
        }

        public static T Create<T>(string name) where T : Component {
            var obj = new GameObject(name, typeof(T));
            return obj.GetComponent<T>();
        }
    
        public static T Create<T>(string name, Vector3 position) where T : Component {
            var created = Create<T>(name);
            created.transform.position = position;
            return created;
        }
    
        public static T Create<T>(string name, Vector3 position, Transform parent) where T : Component {
            var created = Create<T>(name, position);
            created.transform.parent = parent;
            return created;
        }
    
        public static T Create<T>(string name, Transform parent) where T : Component {
            return Create<T>(name, parent.position, parent);
        }
    }
}