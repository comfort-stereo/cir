﻿namespace Utilities.Extensions {
    
    public static class ArrayExtensions {
        
        public static T GetOrNull<T>(this T[] array, int index) where T : class {
            if (index < 0 || index > array.Length - 1) {
                return null;
            }
            return array[index];
        }
        
        public static T MostCommon<T>(this T[] array) {
            var count = 1;
            var popular = array[0];
            for (var i = 0; i < array.Length - 1; i++) {
                var current = array[i];
                var currentCount = 0;
                for (var j = 1; j < array.Length; j++) {
                    if (Equals(current, array[j])) {
                        currentCount++;
                    }
                }
                if (currentCount > count) {
                    popular = current;
                    count = currentCount;
                }
            }
            return popular;
        }
    }
}