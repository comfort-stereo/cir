﻿using System.Collections.Generic;

namespace Utilities.Extensions {
    
    public static class DictionaryExtensions {

        public static R GetOrNull<T, R>(this Dictionary<T, R> dictionary, T key) where R : class {
            R value;
            var found = dictionary.TryGetValue(key, out value);
            return found ? value : null;
        }
    }
}