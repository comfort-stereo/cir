﻿using System.Collections.Generic;

namespace Utilities.Extensions {
    
    public static class ListExtensions {
        
        public static void AddUnique<T>(this List<T> list, T element) {
            if (!list.Contains(element)) {
                list.Add(element);
            }
        }
        
        public static T Pop<T>(this List<T> list) {
            var last = list.Count - 1;
            var element = list[last];
            list.RemoveAt(last);
            return element;
        }

        public static void UnorderedRemoveAt<T>(this List<T> list, int index) {
            var last = list.Count - 1;
            if (index == last) {
                list.RemoveAt(last);
            } else {
                list[index] = list[last];
                list.RemoveAt(last);
            }
        }
    }
}