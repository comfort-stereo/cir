﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Utilities.Extensions {
    
    public static class TypeExtensions {
        
        public static Type GetAbsoluteBaseType(this Type type) {
            var current = type;
            while (current.BaseType != null &&
                   current.BaseType != typeof(MonoBehaviour) &&
                   current.BaseType != typeof(SerializedMonoBehaviour)) {
                current = current.BaseType;
            }
            return current;
        }
    }
}

