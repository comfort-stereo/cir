﻿using System.Text;

namespace Utilities.Extensions {
    
    public static class StringExtensions {
        
        public static string ToSpacedPascalCase(this string text) {
            var builder = new StringBuilder(text.Length + 6);
            for (var i = 0; i < text.Length; i++) {
                var character = text[i];
                builder.Append(character);
                if (char.IsLower(character)) {
                    if (i < text.Length - 1 && char.IsUpper(text[i + 1])) {
                        builder.Append(' ');
                    }
                }
            }
            return builder.ToString();
        }
    }
}