﻿using Priority_Queue;

public class SearchNode<S, A> : FastPriorityQueueNode {
    
    public SearchNode<S, A> Parent { get; private set; }
        public S State { get; private set; }
        public A Action { get; private set; }
    
        public float G { get; private set; }
    public float F {
        get { return this.Priority; }
        set { this.Priority = value; }
    } 
    
    public float H => this.F - this.G;
    
        public SearchNode() : this(null, 0, 0, default(S), default(A)) { }

    public void Initialize(SearchNode<S, A> parent, float g, float f, S state, A action) {
            this.Parent = parent;
            this.G = g;
            this.F = f;
            this.State = state;
            this.Action = action;
    }
    
        public SearchNode(SearchNode<S, A> parent, float gcost, float fcost, S state, A action){
        Initialize(parent, gcost, fcost, state, action);
        }
    
        public int CompareTo(SearchNode<S, A> other) {
            return other.F.CompareTo(this.F);
        }
    }
