﻿using System.Collections.Generic;
using Priority_Queue;
using UnityEngine;
using Utilities.Extensions;
using Utilities.General;

namespace Utilities.Pathfinding {

    public abstract class PathfindingSolver<S, A> {

        SimplePriorityQueue<SearchNode<S, A>> Frontier { get; }
        HashSet<S> ExploredSet { get; } = new HashSet<S>();

        List<SearchNode<S, A>> ActiveNodes { get; } = new List<SearchNode<S, A>>();
        List<SearchNode<S, A>> InactiveNodes { get; } = new List<SearchNode<S, A>>();

        public int MaxNodes { get; protected set; }

        public int NodeCount => this.ActiveNodes.Count + this.InactiveNodes.Count;
        public int ActiveNodeCount => this.ActiveNodes.Count;

        protected PathfindingSolver(int maxNodes) {
            this.MaxNodes = maxNodes;
            this.Frontier = new SimplePriorityQueue<SearchNode<S, A>>();
        }

        void ExpandAndIntegrateNode(SearchNode<S, A> node, S end, ref int count) {
            var expanded = Expand(node.State);

            for (var i = 0; i < expanded.Count; i++) {
                if (count == this.MaxNodes) {
                    return;
                }

                var action = expanded[i];
                var state = ApplyAction(node.State, action);

                if (this.ExploredSet.Contains(state)) {
                    continue;
                }

                var child = CreateNode(node, action, state, end);

                this.Frontier.Enqueue(child, child.F);
                this.ExploredSet.Add(child.State);

                count++;
            }
        }

        protected void Find(S start, S end, List<A> path, out S nearest) {
            var last = GeneratePath(start, end, out nearest);

            Debug.Log("Generated path using " + this.ActiveNodeCount + " nodes.");

            this.Frontier.Clear();
            this.ExploredSet.Clear();
            this.InactiveNodes.AddRange(this.ActiveNodes);
            this.ActiveNodes.Clear();

            BuildSolution(last, path);
        }

        SearchNode<S, A> GeneratePath(S start, S end, out S nearest) {
            var count = 0;
            var first = new SearchNode<S, A>(null, 0, Heuristic(start, end), start, default(A));

            this.Frontier.Enqueue(first, 0);
            this.ExploredSet.Add(first.State);

            var nearestNode = first;

            while (this.Frontier.Count != 0) {
                var node = this.Frontier.Dequeue();
                if (node.H < nearestNode.H) {
                    nearestNode = node;
                }
                if (node.State.Equals(end)) {
                    nearest = node.State;
                    return node;
                }
                ExpandAndIntegrateNode(node, end, ref count);
            }

            nearest = nearestNode.State; 
            
            return null;
        }

        SearchNode<S, A> CreateNode(SearchNode<S, A> parent, A action, S child, S toState) {
            var cost = CostForAction(parent.State, action);

            if (this.InactiveNodes.Count == 0) {
                this.InactiveNodes.Add(new SearchNode<S, A>());
            }

            var node = this.InactiveNodes.Pop();

            this.ActiveNodes.Add(node);

            var g = parent.G + cost;
            var h = Heuristic(child, toState);
            var f = g + h;

            node.Initialize(parent, g, f, child, action);

            return node;
        }

        void BuildSolution(SearchNode<S, A> node, List<A> path) {
            path.Clear();

            if (node == null) {
                return;
            }

            while (node != null) {
                if (!node.Action.Equals(default(A))) {
                    path.Add(node.Action);
                }
                node = node.Parent;
            }
        }
        
        protected abstract float Heuristic(S fromLocation, S toLocation);
        protected abstract List<A> Expand(S position);
        protected abstract float CostForAction(S fromLocation, A action);
        protected abstract S ApplyAction(S location, A action);
    }
}
