﻿using System;
using Utilities.General;

namespace Utilities.Collections {
    
    public class Matrix<T> {
        public int Width { get; }
        public int Height { get; }
        public T[] Values { get; }

        public Matrix(int width, int height) {
            this.Width = width;
            this.Height = height;
            this.Values = new T[this.Width * this.Height];
        }
        
        public Matrix(IntVector2 size) : this(size.X, size.Y) { }

        public T this[int x, int y] {
            get {
                #if UNITY_EDITOR
                if (x < 0 || x >= this.Width || y < 0 || y >= this.Height) {
                    throw new IndexOutOfRangeException("Matrix indices were out of range.");
                }
                #endif
                return this.Values[this.Width * x + y];
            }
            set {
                #if UNITY_EDITOR
                if (x < 0 || x >= this.Width || y < 0 || y >= this.Height) {
                    throw new IndexOutOfRangeException("Matrix indices were out of range.");
                }
                #endif
                this.Values[this.Width * x + y] = value;
            }
        }
        
    }
    
    public static class MatrixExtensions {
        public static T GetOrNull<T>(this Matrix<T> matrix, int x, int y) where T : class {
            if (x >= 0 && y >= 0 && x < matrix.Width && y < matrix.Height) {
                return matrix[x, y];
            }
            return null;
        }
    }
}