﻿using System.Collections.Generic;

namespace Utilities.Collections {
    
    public struct ListView<T> {
        readonly List<T> list; 
        
        static readonly List<T> EmptyList = new List<T>(0);
        
        public static ListView<T> Empty => new ListView<T>(EmptyList); 

        public int Count => this.list.Count;
        
        public ListView(List<T> list) {
            this.list = list;
        }

        public T this[int i] => this.list[i];
    }
}