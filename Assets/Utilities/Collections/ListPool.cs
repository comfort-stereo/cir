﻿using System.Collections.Generic;

namespace Utilities.Collections {
    
    public class ListPool<T> {
        Stack<List<T>> Stack { get; } = new Stack<List<T>>();

        public List<T> Get() {
            if (this.Stack.Count == 0) {
                return new List<T>();
            }
            var list = this.Stack.Pop();
            list.Clear();
            return list;
        }

        public void Return(List<T> list) {
            this.Stack.Push(list);
        }
    }
}