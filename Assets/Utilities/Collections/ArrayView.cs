﻿namespace Utilities.Collections {
    
    public struct ArrayView<T> {
        readonly T[] array; 

        public int Length => this.array.Length;
        
        public ArrayView(T[] array) {
            this.array = array;
        }

        public T this[int i] => this.array[i];
    }
}