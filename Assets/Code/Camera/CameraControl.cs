﻿using System;
using UnityEngine;

public class CameraControl {
    
    const float Height = 100f;
    const float MoveSpeed = 100f;
    const float MaxOrthographicSize = 30;
    const float DefaultOrthographicSize = 24;
    const float MinOrthographicSize = 12;
    
    static Vector3 DefaultRotation => new Vector3(-35f, 135f, 60f);

    Camera Camera { get; }

    public float OrthographicSize => this.Camera.orthographicSize; 

    public CameraControl(Camera camera) {
        this.Camera = camera;
    }

    public void Move(Vector3 vector) {
        this.Camera.transform.position += vector * MoveSpeed * Time.deltaTime;
    }
    
    public void MoveBy(Vector3 vector) {
        this.Camera.transform.position += vector;
    }

    public void ChangeZoom(float delta) {
        var size = this.Camera.orthographicSize; 
        size += size * delta * Time.deltaTime;
        size = Math.Min(size, MaxOrthographicSize);
        size = Math.Max(size, MinOrthographicSize);
        this.Camera.orthographicSize = size;
    }
    
    public bool ScreenPointToPlane(Plane plane, Vector2 screenPoint, out Vector3 point) {
        var ray = this.Camera.ScreenPointToRay(screenPoint);
        float distance;
        if (plane.Raycast(ray, out distance)) {
            point = ray.GetPoint(distance);
            return true;
        }
        point = Vector3.zero;
        return false;
    }

    public bool ViewToPlane(Plane plane, out Vector2 point) {
        var ray = this.Camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        float distance;
        if (plane.Raycast(ray, out distance)) {
            point = ray.GetPoint(distance);
            return true;
        }
        point = Vector3.zero;
        return false;
    }

    public void SetDefaultOrientation() {
        this.Camera.transform.position = new Vector3(0f, 0f, Height); 
        this.Camera.transform.eulerAngles = DefaultRotation;
        this.Camera.orthographicSize = DefaultOrthographicSize;
    }
}
