﻿using System;
using Sirenix.OdinInspector;
using Utilities.General;

public abstract class Block : SerializedMonoBehaviour, IPoolable {
    
    const float FallHeight = 40;

    IntVector3 Position { get; set; }
    public Cell Parent { get; set; }
    
    public int PoolIndex { get; set; }
    public Type PoolCachedType => this.cachedType ?? (this.cachedType = GetType());
    
    public virtual int PoolReserveSize => 1;
    Type cachedType;
    
    public virtual void Instantiate(Cell parent, IntVector3 position, bool falling = false) {
        this.Parent = parent;
        this.transform.position = position + VectorUtility.Higher * FallHeight;
        this.Position = position;
    }
}
