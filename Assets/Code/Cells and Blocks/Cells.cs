﻿using System;
using Application;
using UnityEngine;
using Utilities.General;

public abstract class Cell {
    
    public static readonly Cell[] EmptyNeighborBuffer = new Cell[8];
    public virtual bool RequiresTick => false;
    
    public abstract bool IsDrawn { get; }
    public abstract Type BlockType { get; }
    public bool IsHidden { get; set; }
    
    public bool IsClear => !this.IsDrawn;

    public abstract Block Draw(Vector3 position, Cell[] neighbors);
    public abstract void Clear();
    
    public Block Draw(Vector3 position) => Draw(position, EmptyNeighborBuffer);
    
    public virtual void OnDraw(Cell[] neighbors) { }
    public virtual void Tick(LayerType layerType, IntVector2 position) { }
}

public abstract class BaseCell<T> : Cell where T : Block {
    public override Type BlockType => typeof(T);
}

public abstract class VirtualCell<T> : BaseCell<T> where T : Block {
    protected long RenderId { get; set; } = -1;
    
    public override bool IsDrawn => this.RenderId != -1;
    
    public override Block Draw(Vector3 position, Cell[] neighbors) {
        if (this.IsHidden) {
            Clear();
            return null;
        }
        if (this.IsClear) {
            this.RenderId = Global.RenderManager.Drawer.Create(this.BlockType, position);
        } else {
            Global.RenderManager.Drawer.Change(this.BlockType, this.RenderId, position);
        }
        OnDraw(neighbors);
        
        return null;
    }
    
    public override void Clear() {
        if (this.IsDrawn) {
            Global.RenderManager.Drawer.Destroy(this.BlockType, this.RenderId);
            this.RenderId = -1;
        } 
    }
}

public abstract class HardCell<T> : BaseCell<T> where T : Block {
    protected Block Block { get; set; }

    public override bool IsDrawn => Utility.IsNotNull(this.Block);
    
    public override Block Draw(Vector3 position, Cell[] neighbors) {
        if (this.IsClear) {
            this.Block = Global.RenderManager.Pooler.Get<Block>(this.BlockType);
            this.Block.transform.position = position;
        } 
        OnDraw(neighbors);
        
        return null;
    }
    
    public override void Clear() {
        if (this.IsDrawn) {
            Global.RenderManager.Pooler.Return(this.Block);
            this.Block = null;
        } 
    }
}

public abstract class SimpleCell<T> : VirtualCell<T> where T : Block {
    public override bool RequiresTick => false;
}

public class DirtCell : VirtualCell<DirtBlock> { }
public class StoneCell : VirtualCell<WaterBlock> { }
//public class WaterCell : VirtualCell<WaterBlock> { }
