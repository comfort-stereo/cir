﻿using Sirenix.Serialization;
using UnityEngine;

public class AssetReferenceTable {
    [OdinSerialize] public Mesh CubeMesh { get; private set; } 
    [OdinSerialize] public Mesh QuadMesh { get; private set; } 
    [OdinSerialize] public Mesh PlaneMesh { get; private set; } 
}
