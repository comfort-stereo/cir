﻿using System;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;
using Utilities.Extensions;

public class ResourceManager {
    Dictionary<Type, GameObject> ResourceCache { get; } = new Dictionary<Type, GameObject>();

    public GameObject LoadTypeFromResources(Type type) {
        var obj = this.ResourceCache.GetOrNull(type);

        if (obj == null) {
            var directory = type.GetAbsoluteBaseType().GetNiceName().ToSpacedPascalCase();
            var typename = type.GetNiceName();
            var file = typename.ToSpacedPascalCase();
            var path = directory + "/" + file;
            
            Debug.Log($"Loading type {typename} at resources path {path}.");
            
            obj = (GameObject) Resources.Load(path);

            if (obj == null) {
                Debug.LogError($"Could not find resource of type {typename} at resources path {path}.");
            }
        }
        
        if (obj != null) {
            this.ResourceCache[type] = obj;
            return obj;
        }
        
        return null;
    }
    
    public T LoadTypeFromResources<T>(Type type) where T : Component {
        var obj = LoadTypeFromResources(type);
        if (obj != null) {
            return obj.GetComponent<T>();
        }
        return null;
    }
}
