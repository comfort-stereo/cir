﻿using System;
using System.Collections.Generic;

public class Randomizer {
    
    Random Random { get; set; } = new Random();

    public void Reseed(int seed) {
        this.Random = new Random(seed);
        UnityEngine.Random.InitState(seed);
    }
    
    public int Integer(int min, int max) {
        return this.Random.Next(min, max);
    }

    public float Float(float min, float max) {
        return UnityEngine.Random.Range(min, max);
    }

    public float Ratio() => Float(0f, 1f);

    public T Choice<T>(IList<T> list) {
        if (list.Count == 0) {
            throw new ArgumentException();
        }
        return list[Integer(0, list.Count - 1)];
    }
    
    public bool Chance(float chance) {
        if (chance == 0f) {
            return false;
        }
        if (chance >= 1f) {
            return true;
        }
        return Float(0f, 1f) < chance;
    }
}
