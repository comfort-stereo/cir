﻿using Utilities.General;

public struct WorldGenerationParameters {
    public IntVector2 Size { get; }

    public WorldGenerationParameters(IntVector2 size) {
        this.Size = size;
    }
}
