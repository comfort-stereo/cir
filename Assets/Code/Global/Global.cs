﻿namespace Application {
    
    public static class Global {
        public static World World { get; internal set; }
        public static RenderManager RenderManager { get; private set; }
        public static Observer Observer { get; private set; }
        public static JobHandler JobHandler { get; private set; } 
        public static AssetReferenceTable AssetReferenceTable { get; private set; }
        public static ResourceManager ResourceManager { get; private set; }
        public static Randomizer Randomizer { get; private set; }
        public static PathfindingSystem PathfindingSystem { get; private set; }

        public static void Initialize(AssetReferenceTable assetReferenceTable) {
            World = new World();
            RenderManager = new RenderManager();
            JobHandler = new JobHandler();
            Observer = new Observer();
            AssetReferenceTable = assetReferenceTable; 
            ResourceManager = new ResourceManager();
            Randomizer = new Randomizer();
            PathfindingSystem = new PathfindingSystem();
        }
    }
}