﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Utilities;
using Utilities.General;

namespace Application {
    
    public class Game : SerializedMonoBehaviour {
        
        [OdinSerialize, Required] AssetReferenceTable AssetReferenceTable { get; } = new AssetReferenceTable();
        
        void Awake() {
            Initialize();
        }

        void Initialize() {
            Global.Initialize(this.AssetReferenceTable);
            MokGenerateWorld();
        }

        void Update() {
            Global.World.Update();
            Global.JobHandler.Update();
            Global.Observer.Update();
            Global.RenderManager.Render();
        }

        void MokGenerateWorld() {
            Global.World = new World(new WorldGenerationParameters(IntVector2.One * 200));
            
            var ground = Global.World.GetLayer(LayerType.Ground);
            var surface = Global.World.GetLayer(LayerType.Surface);
            
            var start = IntVector2.One * 5;
            var entity1 = Global.World.CreateEntity<Settler>(start);
//            var entity2 = Global.World.CreateEntity<Settler>(start + IntVector.One * 10);
            
            ground.IteratePositions(position => {
                ground.PutCell(position, new DirtCell());
            });
            surface.IteratePositions(position => {
                if (Global.Randomizer.Chance(0.05f)) {
                    if (Global.World.GetEntities(position).Count == 0) {
                        surface.PutCell(position, new StoneCell());
                    }
                }
            });
            
            Global.World.GetLayer(LayerType.Surface).PutCell(new IntVector2(0, 0),  new StoneCell());
            Global.RenderManager.Pooler.Reserve<DirtBlock>(8000);
            Global.RenderManager.Pooler.Reserve<StoneBlock>(4000);

//            var x = 0;
//            var y = 0;
            
//            Global.JobHandler.DoInterval(0.1f, () => {
//                if (y == 200) {
//                    x++;
//                    y = 0;
//                }
//                if (x == 100) {
//                    return true;
//                }
//                
//                Global.World.PutCellAnimated(LayerType.Surface, new IntVector(x, y), new StoneCell());
//                
//                y++;
//                
//                return false;
//            });

            var end = start + IntVector2.N * 20 + IntVector2.E * 20; 
            
//            for (var i = -10; i <= 10; i++) {
//                Global.World.RemoveGroundCell(start + IntVector.E * i + IntVector.N * 10);
//            }
//            
//            for (var i = -10; i <= 10; i++) {
//                Global.World.RemoveGroundCell(start + IntVector.N * i + IntVector.E * 10);
//            }
//            var entity2 = Global.World.CreateEntity<Settler>(start + IntVector.Down);
            
//            entity1.Move(end);
//            entity2.Move(end);
        }

//        void OnGUI() {
//            GUI.BeginGroup(new Rect(0f, 0f, 100f, 100f));
//            GUI.Label(new Rect(0f, 0f, 100f, 20f), (1 / Time.deltaTime).ToString());
//            GUI.EndGroup();
//        }
        
        public void New(WorldGenerationParameters parameters) {
            Global.World = new World(parameters);
        }
    }
}
