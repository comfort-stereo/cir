﻿using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Extensions;

public class JobHandler {
    
    public delegate bool Job();

    class IntervalJob {
        public readonly Job Job;
        public readonly float Interval;
        public float Remaining;

        public IntervalJob(Job job, float interval) {
            this.Job = job;
            this.Interval = interval;
            this.Remaining = interval;
        }

        public bool Update(float delta) {
            this.Remaining = this.Remaining - delta;

            while (this.Remaining <= 0f) {
                if (this.Job()) {
                    return true;
                }
                this.Remaining += this.Interval;
            }

            return false;
        }
    } 

    List<Job> Jobs { get; } = new List<Job>();
    List<IntervalJob> IntervalJobs { get; } = new List<IntervalJob>();
    List<int> FinishedJobsIndicesBuffer { get; } = new List<int>();

    public void Update() {
        UpdateJobs();
        UpdateIntervalJobs();
    }

    void UpdateJobs() {
        for (var i = 0; i < this.Jobs.Count; i++) {
            if (this.Jobs[i]()) {
                this.FinishedJobsIndicesBuffer.Add(i);
            }
        }

        if (this.FinishedJobsIndicesBuffer.Count != 0) {
            ClearListIndices(this.Jobs, this.FinishedJobsIndicesBuffer);
        }
    }

    void UpdateIntervalJobs() {
        var delta = Time.deltaTime;
        for (var i = 0; i < this.IntervalJobs.Count; i++) {
            if (this.IntervalJobs[i].Update(delta)) {
                this.FinishedJobsIndicesBuffer.Add(i);
            }
        }
        
        if (this.FinishedJobsIndicesBuffer.Count != 0) {
            ClearListIndices(this.IntervalJobs, this.FinishedJobsIndicesBuffer);
        }
    }

    static void ClearListIndices<T>(List<T> list, List<int> indices) {
        for (var i = indices.Count - 1; i >= 0; i--) {
            list.UnorderedRemoveAt(indices[i]);
        }
        indices.Clear();
    }

    public void Do(Job job) {
        this.Jobs.Add(job);
    }

    public void DoInterval(float interval, Job job) {
        this.IntervalJobs.Add(new IntervalJob(job, interval));
    }

    public void Clear() {
        this.Jobs.Clear();
        this.IntervalJobs.Clear();
    }
}
