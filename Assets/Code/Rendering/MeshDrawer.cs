﻿using System;
using System.Collections.Generic;
using Application;
using UnityEngine;
using Utilities.Extensions;

public class MeshDrawer {
    
    static Quaternion DefaultRotation => Quaternion.identity;
    static Vector3 DefaultScale => Vector3.one;
    
    Dictionary<Type, MeshDrawerEntry> Entries { get; } = new Dictionary<Type, MeshDrawerEntry>();
    
    Type LastType { get; set; }
    MeshDrawerEntry LastMeshDrawerEntry { get; set; }
    
    public long Create(Type type, Vector3 position) {
        return GetOrCreateEntry(type).Register(position, DefaultRotation, DefaultScale);
    }

    public void Change(Type type, long id, Vector3 position) {
        GetOrCreateEntry(type).Change(id, position, DefaultRotation, DefaultScale);
    }
    
    public void Destroy(Type type, long id) {
        GetOrCreateEntry(type).Destroy(id);
    }

    public void Draw() {
        foreach (var entry in this.Entries.Values) {
            entry.Draw();
        }
    }
    
    MeshDrawerEntry GetOrCreateEntry(Type type) {
        if (ReferenceEquals(type, this.LastType)) {
            return this.LastMeshDrawerEntry;
        }
        
        var entry = this.Entries.GetOrNull(type);
        
        if (entry == null) {
            var obj = Global.ResourceManager.LoadTypeFromResources(type);
            var filter = obj.GetComponent<MeshFilter>();
            var renderer = obj.GetComponent<MeshRenderer>();
            entry = new MeshDrawerEntry(filter.sharedMesh, renderer.sharedMaterial);
            this.Entries[type] = entry;
        }
        
        this.LastType = type;
        this.LastMeshDrawerEntry = entry;
        
        return entry;
    }
}
