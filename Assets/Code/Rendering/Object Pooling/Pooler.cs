﻿using System;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;
using Utilities;
using Utilities.Extensions;
using Utilities.General;

public class Pooler {
    
    Dictionary<Type, Pool> Pools { get; } = new Dictionary<Type, Pool>();
    
    Transform Root { get; }
    
    Type LastType { get; set; }
    Pool LastPool { get; set; }

    public int Size {
        get {
            var count = 0;
            foreach (var pair in this.Pools) {
                count += pair.Value.Size;
            }
            return count;
        }
    }

    public Pooler(Transform root) {
        this.Root = root;
    }

    public void Reserve(Type type, int count) {
        var pool = GetOrAddPool(type);
        pool.Reserve(count);
    }

    public void Reserve<T>(int count) where T : IPoolable {
        Reserve(typeof(T), count);
    }
    
    public T Get<T>(Type type) where T : IPoolable {
        var pool = GetOrAddPool(type);
        return (T) pool.Get();
    }
    
    public T Get<T>() where T : IPoolable {
        return Get<T>(typeof(T));
    }

    public void Return(IPoolable obj) {
        var pool = GetOrAddPool(obj.PoolCachedType);
        pool.Return(obj);
    }

    Pool GetOrAddPool(Type type) {
        if (ReferenceEquals(type, this.LastType)) {
            return this.LastPool;
        }
        var pool = this.Pools.GetOrNull(type) ?? AddPool(type);
        this.LastType = type;
        this.LastPool = pool;
        return pool;
    }

    Pool AddPool(Type type) {
        var name = type.GetNiceName().SplitPascalCase();
        var subroot = ObjectExtensions.CreateTransform(name, this.Root);
        var pool = new Pool(type, subroot);
        this.Pools[type] = pool;
        return pool;
    }
}
