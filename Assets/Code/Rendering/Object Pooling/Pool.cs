﻿using System;
using System.Collections.Generic;
using Application;
using UnityEngine;
using Utilities.Extensions;
using Object = UnityEngine.Object;

class Pool {
    
    GameObject Prefab { get; }
    Transform Root { get; }
    
    List<IPoolable> InactiveBuffer { get; } = new List<IPoolable>();

    public int Size => this.InactiveBuffer.Count; 

    public Pool(Type type, Transform root) {
        this.Prefab = Global.ResourceManager.LoadTypeFromResources(type);
        this.Root = root;
    }
    
    public IPoolable Get() {
        IPoolable instance;
        if (this.InactiveBuffer.Count == 0) {
            instance = Create(true);
            Reserve(instance.PoolReserveSize - 1);
        } else {
            instance = this.InactiveBuffer.Pop(); 
            instance.gameObject.SetActive(true);
        }
        return instance;
    }

    public void Return(IPoolable instance) {
        this.InactiveBuffer.Add(instance);
        instance.gameObject.SetActive(false);
    }

    public void Reserve(int count) {
        for (var i = 0; i < count; i++) {
            Create();
        }
    }
    
    IPoolable Create(bool active = false) {
        var obj = Object.Instantiate(this.Prefab);
        
        obj.transform.parent = this.Root;
        obj.SetActive(active);
        
        var instance = obj.GetComponent<IPoolable>();
        
        if (!active) {
            this.InactiveBuffer.Add(instance);
        }
        
        return instance;
    }
}

