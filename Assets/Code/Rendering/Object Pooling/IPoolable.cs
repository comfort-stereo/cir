﻿using System;
using UnityEngine;

public interface IPoolable {
    int PoolReserveSize { get; }
    Type PoolCachedType { get; }
    GameObject gameObject { get; }
}

