﻿using System.Collections.Generic;
using UnityEngine;

class MeshDrawerEntry {
    
    struct Location {
        public readonly Vector3 Position;
        public readonly Quaternion Rotation;
        public readonly Vector3 Scale;
        
        public Location(Vector3 position, Quaternion rotation, Vector3 scale) {
            this.Position = position;
            this.Rotation = rotation;
            this.Scale = scale;
        }

        public Matrix4x4 ToMatrix4x4() {
            return Matrix4x4.TRS(this.Position, this.Rotation, this.Scale);
        }
    }
    
    const int LocationBufferSize = 1023;
    const int InitialLocationTableSize = 1000;
    
    Mesh Mesh { get; }
    Material Material { get; }
    
    long NextRenderId { get; set; }

    long GetNextRenderId() {
        return this.NextRenderId++;
    }
    
    Dictionary<long, Location> LocationTable { get; } = new Dictionary<long, Location>(InitialLocationTableSize);
    List<Matrix4x4[]> LocationBuffers { get; } = new List<Matrix4x4[]>();
    
    bool NeedsUpdate { get; set; }

    public MeshDrawerEntry(Mesh mesh, Material material) {
        this.Mesh = mesh;
        this.Material = material;
    }

    public void Draw() {
        if (this.NeedsUpdate) {
            Update();
        }

        var count = this.LocationTable.Count;
        var lastBufferIndex = count / LocationBufferSize;
        var lastBufferCount = count - lastBufferIndex * LocationBufferSize;

        for (var i = 0; i < lastBufferIndex; i++) {
            Graphics.DrawMeshInstanced(this.Mesh, 0, this.Material, this.LocationBuffers[i]);
        }
        Graphics.DrawMeshInstanced(this.Mesh, 0, this.Material, this.LocationBuffers[lastBufferIndex], lastBufferCount);
    }

    public void Update() {
        this.NeedsUpdate = false;
        PopulateLocationBuffers();
    }

    void PopulateLocationBuffers() {
        var inBufferIndex = 0;
        var index = 0;
        
        foreach (var pair in this.LocationTable) {
            var bufferIndex = index / LocationBufferSize;
            if (bufferIndex >= this.LocationBuffers.Count) {
                this.LocationBuffers.Add(new Matrix4x4[LocationBufferSize]);
            }
            this.LocationBuffers[bufferIndex][inBufferIndex] = pair.Value.ToMatrix4x4();
            inBufferIndex = (inBufferIndex + 1) % LocationBufferSize;
            index++;
        }
    }

    public long Register(Vector3 position, Quaternion rotation, Vector3 scale) {
        var id = GetNextRenderId();
        this.LocationTable[id] = new Location(position, rotation, scale);
        this.NeedsUpdate = true;
        return id;
    }

    public void Change(long id, Vector3 position, Quaternion rotation, Vector3 scale) {
        this.LocationTable[id] = new Location(position, rotation, scale);
        this.NeedsUpdate = true;
    }

    public void Destroy(long id) {
        this.LocationTable.Remove(id);
        this.NeedsUpdate = true;
    }
}
