﻿using System;
using System.Collections.Generic;
using Application;
using UnityEngine;
using Utilities.Extensions;
using Utilities.General;

public class RenderManager {
    
	IntRectangle LastRenderBox { get; set; } 
	
	public Pooler Pooler { get; }
	public MeshDrawer Drawer { get; }

	bool RenderSectionsNeedToBeInitialized { get; set; } = true;
	
	List<IntVector2> OutputRenderSections { get; } = new List<IntVector2>(); 
	List<IntVector2> OutputClearedSections { get; } = new List<IntVector2>();
	
	public RenderManager() {
		this.Pooler = new Pooler(ObjectExtensions.CreateTransform("Pooling Root"));
		this.Drawer = new MeshDrawer(); 
	}

	IntRectangle CurrentRenderBox { get; set; }

	public void SetRenderBounds(Vector2 position, int distance) {
		this.CurrentRenderBox = CalculateRenderBox(new IntVector2(position), distance);
	}

	public bool IsVisible(IntVector2 position) {
		var renderSection = new IntVector2(position.X / Layer.RenderSectionSize, position.Y / Layer.RenderSectionSize);
		return this.CurrentRenderBox.Contains(renderSection);
	}

	public void Render() {
		var render = UpdateRenderSections();
		if (render) {
			for (var i = 0; i < World.LayerCount; i++) {
				var layer = Global.World.GetLayer(i);
				layer.Draw(this.OutputRenderSections, false);
				layer.Draw(this.OutputClearedSections, true);
			}
		}
		this.Drawer.Draw();
	}

	bool UpdateRenderSections() {
		
		var current = this.CurrentRenderBox;
		var last = this.LastRenderBox;

		if (this.RenderSectionsNeedToBeInitialized) {
            for (var x = current.Start.X; x <= current.End.X; x++) {
                for (var y = current.Start.Y; y <= current.End.Y; y++) {
                    var section = new IntVector2(x, y);
                    this.OutputRenderSections.Add(section);
                }
            }
			this.RenderSectionsNeedToBeInitialized = false;
			this.LastRenderBox = this.CurrentRenderBox;
			return true;
		}
		
		if (current == last) {
			return false;
		}

        this.OutputRenderSections.Clear();
		for (var x = current.Start.X; x <= current.End.X; x++) {
			for (var y = current.Start.Y; y <= current.End.Y; y++) {
				var section = new IntVector2(x, y);
				if (last.Contains(section)) {
					continue;
				}
                this.OutputRenderSections.Add(section);
			}
		}
		
        this.OutputClearedSections.Clear();
		for (var x = last.Start.X; x <= last.End.X; x++) {
			for (var y = last.Start.Y; y <= last.End.Y; y++) {
				var section = new IntVector2(x, y);
				if (current.Contains(section)) {
					continue;
				}
                this.OutputClearedSections.Add(section);
			}
		}

		this.LastRenderBox = current;

		return true;
	}

	static IntRectangle CalculateRenderBox(IntVector2 position, int size) {
		var world = Global.World;
		return new IntRectangle(
			Math.Max((position.X - size) / Layer.RenderSectionSize, 0),
			Math.Max((position.Y - size) / Layer.RenderSectionSize, 0),
			Math.Min((position.X + size) / Layer.RenderSectionSize, world.Size.X / Layer.RenderSectionSize - 1),
			Math.Min((position.Y + size) / Layer.RenderSectionSize, world.Size.Y / Layer.RenderSectionSize - 1));
	}
}
