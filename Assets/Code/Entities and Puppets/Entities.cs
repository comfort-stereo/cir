﻿using System;
using Application;
using Utilities.General;

public abstract class Entity {
    
    protected Pathfinder Pathfinder { get; private set; } 
    
    /*
     * Do not modify from inside an Entity. This gets set by entity manager.
     */
    public IntVector2 ManagedPosition { get; set; }

    public IntVector2 Position => this.ManagedPosition; 
    
    public IntVector2 LastPosition { get; protected set; }
    public IntVector2 Size { get; protected set; }
    public Direction Facing { get; protected set; }
    
    public abstract int MoveDelay { get; } 
    public abstract Puppet Puppet { get; set; }
    public abstract Type PuppetType { get; }
    
    public int TicksUntilAction { get; set; }
    public Direction MoveDirection { get; set; }
    
    protected abstract void OnTick();
    
    public void Initialize(IntVector2 position) {
        this.Pathfinder = new Pathfinder(CanMove);
        SetPosition(position);
    }
    
    public void Tick() {
        this.TicksUntilAction = Math.Max(0, this.TicksUntilAction - 1);

        if (this.Pathfinder.HasPath) {
            if (this.TicksUntilAction == 0) {
                var direction = this.Pathfinder.Next(this.Position);
                MoveImmediate(direction);
                this.TicksUntilAction = direction.IsDiagonal() ? (int) (this.MoveDelay * Constant.Sqrt2) : this.MoveDelay;
            } 
        }
        
        OnTick();
    }

    public void SetPosition(IntVector2 position) {
        Global.World.PutEntity(this, position);
        this.LastPosition = position;
    }
    
    public void Move(IntVector2 position) {
        this.Pathfinder.Navigate(this.Position, position);
    }
    
    public void Draw() {
        if (this.Puppet == null) {
            this.Puppet = Global.RenderManager.Pooler.Get<Puppet>(this.PuppetType);
            this.Puppet.Attach(this);
        }
    }

    public void Destroy() {
        if (this.Puppet != null) {
            Global.RenderManager.Pooler.Return(this.Puppet);
            this.Puppet = null;
        }
    }
    
    void MoveImmediate(Direction direction) {
        this.LastPosition = this.Position;
        Global.World.MoveEntity(this, direction.ToIntVector() + this.Position);
    }

    bool CanStandOn(Slice slice) {
        if (slice.GroundCell == null) {
            return false;
        }
        if (slice.SurfaceCell != null) {
            return false;
        }
        if (slice.Entities.Count != 0) {
            return false;
        }
        return true;
    }

    bool NothingBlocks(Slice slice) {
        if (slice.SurfaceCell != null) {
            return false;
        }
        if (slice.Entities.Count != 0) {
            return false;
        }
        return true;
    }

    bool CanStandAt(IntVector2 position) {
        return CanStandOn(Global.World.GetSlice(position));
    }

    bool NothingBlockingAt(IntVector2 position) {
        return NothingBlocks(Global.World.GetSlice(position));
    }

    bool CanMove(IntVector2 position, Direction direction) {
        var next = position + direction.ToIntVector();
        if (direction.IsDiagonal()) {
            var a = position + (direction == Direction.NE || direction == Direction.NW ? IntVector2.N : IntVector2.S);
            var b = position + (direction == Direction.NE || direction == Direction.SE ? IntVector2.E : IntVector2.W);
            return CanStandAt(next) && NothingBlockingAt(a) && NothingBlockingAt(b);
        } 
        return CanStandAt(next);
    }
}

public abstract class BaseEntity<T> : Entity where T : Puppet {
    public override Type PuppetType => typeof(T);
    
    protected T TypedPuppet { get; set; }

    public override Puppet Puppet {
        get { return this.TypedPuppet; }
        set { this.TypedPuppet = (T) value; }
    }
}

public class Settler : BaseEntity<SettlerPuppet> {
    public override int MoveDelay => 4;

    protected override void OnTick() { }
}

