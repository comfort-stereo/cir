﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities.General;

[RequireComponent(typeof(Animator))]
public abstract class Puppet : SerializedMonoBehaviour, IPoolable {
    public int PoolIndex { get; set; }
    public virtual int PoolReserveSize { get; } = 1;
    public Type PoolCachedType { get; private set; }
    
    public const float MoveSpeed = 40f; 
    
    public abstract Entity Entity { get; set; }

    public abstract void Attach(Entity entity);
}

public abstract class BasePuppet<T> : Puppet where T : Entity {
    
    protected T TypedEntity { get; set; }

    public override Entity Entity {
        get { return this.TypedEntity; }
        set { this.TypedEntity = (T) value; }
    }

    public override void Attach(Entity entity) {
        this.TypedEntity = (T) entity;
        this.transform.position = entity.LastPosition;
    }

    void Update() {
        if (Utility.IsNotNull(this.Entity)) {
            Imitate();
        }
    }

    void Imitate() {
        this.transform.position = Vector3.MoveTowards(
            this.transform.position,
            this.Entity.Position,
            MoveSpeed * Time.deltaTime
        );
    }
}
