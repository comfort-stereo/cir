﻿using System.Collections.Generic;
using Application;
using UnityEngine;
using Utilities;
using Utilities.Extensions;
using Utilities.General;

public class Observer {

    const float OrthographicSizeToRenderDistance = 3.3f;

    Plane WorldPlane => new Plane(VectorUtility.Higher, VectorUtility.Zero);
    
    CameraControl CameraControl { get; }
    
    List<Entity> SelectedEntities { get; } = new List<Entity>();

    public Observer() {
        this.CameraControl = new CameraControl(Object.FindObjectOfType<Camera>());
        this.CameraControl.SetDefaultOrientation();
        Cursor.visible = true;
        LookAt(new Vector2(0, 0));
    }

    public void LookAt(Vector2 position) {
        this.CameraControl.MoveBy(position - GetLookPosition());
    }
    
    public void Update() {
        HandleInput();
        Render();
    }

    void HandleInput() {
        var x = Input.GetAxis("Horizontal") * (Vector2.up + Vector2.left).normalized;
        var y = Input.GetAxis("Vertical") * (Vector2.up + Vector2.right).normalized;
        
        this.CameraControl.Move(x + y);
        this.CameraControl.ChangeZoom(-Input.mouseScrollDelta.y);

        if (Input.GetMouseButtonDown(0)) {
            var slice = GetSliceFromMouse();
            if (slice.Entities.Count != 0) {
                this.SelectedEntities.Clear();
                this.SelectedEntities.AddUnique(slice.Entities[0]);
            }
        }
        if (Input.GetMouseButtonDown(1)) {
            var slice = GetSliceFromMouse();
            if (this.SelectedEntities.Count != 0) {
                foreach (var entity in this.SelectedEntities) {
                    entity.Move(slice.Position);
                }
            }
        }
    }

    Slice GetSliceFromMouse() {
        Vector3 point;
        this.CameraControl.ScreenPointToPlane(this.WorldPlane, Input.mousePosition, out point);
        var position = new IntVector2(point + IntVector2.One); 
        var slice = Global.World.GetSlice(position);
        Debug.DrawLine(position - VectorUtility.Higher, position + VectorUtility.Higher * 10f, Color.blue, 2);
        return slice;
    }

    Vector2 GetLookPosition() {
        Vector2 point;
        this.CameraControl.ViewToPlane(this.WorldPlane, out point);
        return point;
    }

    void Render() {
        var point = GetLookPosition();
        var distance = this.CameraControl.OrthographicSize * OrthographicSizeToRenderDistance;
        Global.RenderManager.SetRenderBounds(point, (int) distance);
    }
}
