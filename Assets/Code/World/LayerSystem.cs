﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using Utilities;
using Utilities.Extensions;
using Utilities.General;

public class LayerSystem {
	
	public IntVector2 Size { get; }
	
    Layer[] Layers { get; }
	
	List<TickedCellEntry> TickedCellEntries { get; } = new List<TickedCellEntry>();

	struct TickedCellEntry {
		public readonly Cell Cell;
		public readonly LayerType LayerType;
		public readonly IntVector2 Position;
		
		public TickedCellEntry(Cell cell, LayerType layerType, IntVector2 position) {
			this.Cell = cell;
			this.LayerType = layerType;
			this.Position = position;
		}
	}

    public LayerSystem(int layerCount, IntVector2 size) {
	    this.Size = size;
        this.Layers = new Layer[layerCount];
		for (var i = 0; i < this.Layers.Length; i++) {
			this.Layers[i] = new Layer(size, i - 1);
		}
    }

	public void Tick() {
		foreach (var entry in this.TickedCellEntries) {
			entry.Cell.Tick(entry.LayerType, entry.Position);
		}
	}
	
	public void SetLayer(LayerType type, Layer layer) {
		this.Layers[(int) type] = layer;
	}

	public Layer GetLayer(int index) => this.Layers[index];
	public Layer GetLayer(LayerType type) => this.Layers[(int) type];
	
	public Cell GetCell(LayerType type, IntVector2 position) => GetLayer(type).GetCellBounded(position);
	public void PutCell(LayerType type, IntVector2 position, Cell cell) {
		RegisterCell(cell, type, position);
		GetLayer(type).PutCell(position, cell);
	}

	public void PutCellAnimated(LayerType type, IntVector2 position, Cell cell, CellChangeAnimation animation) {
		RegisterCell(cell, type, position);
		GetLayer(type).PutCellAnimated(position, cell, animation);
	}

	public void RegisterCell(Cell cell, LayerType type, IntVector2 position) {
		if (cell.RequiresTick) {
			this.TickedCellEntries.AddUnique(new TickedCellEntry(cell, type, position));
		}
	}

	public void RemoveCell(LayerType type, IntVector2 position) {
		var cell = GetCell(type, position);
		if (cell == null) {
			return;
		}
		if (cell.RequiresTick) {
			var index = this.TickedCellEntries.FindIndex(entry => entry.Cell == cell);
			if (index >= 0) {
                this.TickedCellEntries.UnorderedRemoveAt(index);
			}
		}
		GetLayer(type).RemoveCell(position);
	}
}
