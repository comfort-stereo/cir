﻿using System;
using System.Collections.Generic;
using Application;
using UnityEngine;
using Utilities;
using Utilities.Collections;
using Utilities.General;

public class Layer {

	public const float BlockFallHeight = 80f;
	public const float InitialBlockFallSpeed = 80f;
	public const float BlockFallAcceleration = 10f;
	public const float BlockInflateTime = 0.05f;
	public const float BlockFallSpeed = 25f;
	public const int RenderSectionSize = 10;
	
	public delegate void PositionIterationFunction(IntVector2 position);
	
	public IntVector2 Size { get; } 
	public int Height { get; }

	Cell[] NeighborBuffer { get; } = new Cell[8];
	
	Matrix<Cell> Matrix { get; }

	public Layer(IntVector2 size, int height) {
		this.Size = size;
		this.Height = height;
		this.Matrix = new Matrix<Cell>(size.X, size.Y);
	}

	public Cell GetCell(int x, int y) => this.Matrix[x, y];
	public Cell GetCell(IntVector2 position) => GetCell(position.X, position.Y);
	public Cell GetCellBounded(int x, int y) => this.Matrix.GetOrNull(x, y);
	public Cell GetCellBounded(IntVector2 position) => GetCellBounded(position.X, position.Y);

	public void PutCell(int x, int y, Cell cell) {
		this.Matrix[x, y] = cell;
	}
	
	public void PutCell(IntVector2 position, Cell cell) {
		PutCell(position.X, position.Y, cell);
	}
	
	public void RemoveCell(int x, int y) {
		var cell = GetCell(x, y);
		if (cell == null) {
			return;
		}
		cell.Clear();
		PutCell(x, y, null);
	}

	public void RemoveCell(IntVector2 position) {
		RemoveCell(position.X, position.Y);
	}

	public void Draw(List<IntVector2> sections, bool clear) {
		for (var i = 0; i < sections.Count; i++) {
			DrawSection(sections[i], clear);
		}
	}

	public void IteratePositions(PositionIterationFunction function) {
		var size = this.Size;
		for (var x = 0; x < size.X; x++) {
			for (var y = 0; y < size.Y; y++) {
				function(new IntVector2(x, y));
			}
		}
	}
	
	public void GetNeighbors(IntVector2 position, Cell[] buffer) {
		var i = 0;
		for (var x = -1; x <= 1; x++) {
			for (var y = -1; y <= 1; y++) {
				if (x != 0 || y != 0) {
					buffer[i++] = GetCellBounded(position.X + x, position.Y + y);
				}
			}
		}
	}
	
	public void DrawSection(IntVector2 section, bool clear) {
		var start = section * RenderSectionSize;
		var end = start + IntVector2.One * RenderSectionSize;
		var height = this.Height; 
        for (var x = start.X; x < end.X; x++) {
            for (var y = start.Y; y < end.Y; y++) {
                var cell = GetCell(x, y);
                if (cell == null) {
                    continue;
                }
                var position3D = new Vector3(x, y, height);
	            var position2D = new IntVector2(x, y);
                if (clear) {
                    cell.Clear();
                } else {
                    GetNeighbors(position2D, this.NeighborBuffer);
                    cell.Draw(position3D, this.NeighborBuffer);
                }
            }
        }
	}

	public void PutCellAnimated(IntVector2 position, Cell cell, CellChangeAnimation animation) {
		switch (animation) {
			case CellChangeAnimation.Fall:
				PutCellWithFallAnimation(position, cell);
				break;
			case CellChangeAnimation.Inflate:
				PutCellWithInflateAnimation(position, cell);
				break;
			default:
				throw new ArgumentOutOfRangeException(nameof(animation), animation, null);
		}
	}
	
	void PutCellWithFallAnimation(IntVector2 position, Cell cell) {
		PutCell(position.X, position.Y, cell);
		
		var block = Global.RenderManager.Pooler.Get<Block>(cell.BlockType);
		var destination = new Vector3(position.X, position.Y, this.Height);
		var start = new Vector3(position.X, position.Y, this.Height + BlockFallHeight);

		block.transform.position = start; 
		cell.IsHidden = true;
		
		var velocity = VectorUtility.Lower * InitialBlockFallSpeed;
		
		Global.JobHandler.Do(() => {

			velocity += VectorUtility.Lower * 10f * Time.deltaTime;
			block.transform.position += velocity * Time.deltaTime;

			if (block.transform.position.z <= destination.z) {
				block.transform.position = destination;
			}

			var finished = block.transform.position == destination; 
			if (finished) {
				cell.IsHidden = false;
				if (Global.RenderManager.IsVisible(position)) {
					cell.Draw(new IntVector3(destination));
				} 
				Global.RenderManager.Pooler.Return(block);
			} 
			
			return finished;
		});
		
	}
	
	void PutCellWithInflateAnimation(IntVector2 position, Cell cell) {
		PutCell(position.X, position.Y, cell);
		
		var block = Global.RenderManager.Pooler.Get<Block>(cell.BlockType);
		var originalScale = block.transform.localScale;
		var startScale = Vector3.one * 0.1f;
		var absolutePosition =  new Vector3(position.X, position.Y, this.Height);
		
		block.transform.position = absolutePosition; 
		block.transform.localScale = startScale;
		cell.IsHidden = true;

		var delta = (originalScale.x - startScale.x) / BlockInflateTime;
		
		Global.JobHandler.Do(() => {
			block.transform.localScale = Vector3.MoveTowards(block.transform.localScale, originalScale, delta * Time.deltaTime);
			
			var finished = block.transform.localScale == originalScale; 
			if (finished) {
				cell.IsHidden = false;
				if (Global.RenderManager.IsVisible(position)) {
					cell.Draw(new IntVector3(absolutePosition));
				} 
				Global.RenderManager.Pooler.Return(block);
			} 
			
			return finished;
		});
	}
}

public enum CellChangeAnimation {
	Fall,
	Inflate
}
