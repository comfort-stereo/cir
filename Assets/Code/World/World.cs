﻿using System;
using UnityEngine;
using Utilities;
using Utilities.Collections;
using Utilities.General;

public enum LayerType {
	Ground,
	Surface,
	Roof
}

public class World {
	
	public const int DefaultTicksPerSecond = 30;
	public const int LayerCount = 3;

	public IntVector2 Size => this.LayerSystem.Size; 
	
	public float TimeUntilTick { get; set; }
	
	public WorldSettings Settings { get; } = new WorldSettings();
	
	EntitySystem EntitySystem { get; } = new EntitySystem();
	LayerSystem LayerSystem { get; } 
	
	public World(WorldGenerationParameters parameters) {
		this.LayerSystem = new LayerSystem(LayerCount, parameters.Size);
	}
	
	public World() : this(new WorldGenerationParameters(IntVector2.Zero)) { }
	
	public void Update() {
        this.TimeUntilTick = Math.Max(0f, this.TimeUntilTick - Time.deltaTime);
		if (this.TimeUntilTick == 0f) {
			Tick();
			this.TimeUntilTick = 1f / (DefaultTicksPerSecond * this.Settings.TickSpeed);
		}
	} 

	void Tick() {
		this.LayerSystem.Tick();
		this.EntitySystem.Tick();
	}
	
	/*
	 * LayerSystem delegation
	 */
	public Layer GetLayer(int index) => this.LayerSystem.GetLayer(index);
	public Layer GetLayer(LayerType type) => this.LayerSystem.GetLayer(type);
	
	public void PutCellAnimated(LayerType type, IntVector2 position, Cell cell) {
		this.LayerSystem.PutCellAnimated(type, position, cell, CellChangeAnimation.Inflate);
	}
	
	public Cell GetCell(LayerType type, IntVector2 position) => this.LayerSystem.GetCell(type, position);
	public Cell GetGroundCell(IntVector2 position) => GetCell(LayerType.Ground, position);
	public Cell GetSurfaceCell(IntVector2 position) => GetCell(LayerType.Surface, position);
	public Cell GetRoofCell(IntVector2 position) => GetCell(LayerType.Roof, position);
	
	public void PutCell(LayerType type, IntVector2 position, Cell cell) => this.LayerSystem.PutCell(type, position, cell);
	public void PutGroundCell(IntVector2 position, Cell cell) => PutCell(LayerType.Ground, position, cell);
	public void PutSurfaceCell(IntVector2 position, Cell cell) => PutCell(LayerType.Surface, position, cell);
	public void PutRoofCell(IntVector2 position, Cell cell) => PutCell(LayerType.Roof, position, cell);
	
	public void RemoveCell(LayerType type, IntVector2 position) => this.LayerSystem.RemoveCell(type, position);
	public void RemoveGroundCell(IntVector2 position) => this.LayerSystem.RemoveCell(LayerType.Ground, position);
	public void RemoveSurfaceCell(IntVector2 position) => this.LayerSystem.RemoveCell(LayerType.Surface, position);
	public void RemoveRoofCell(IntVector2 position) => this.LayerSystem.RemoveCell(LayerType.Roof, position);
	
	/*
	 * EntitySystem delegation
	 */ 
	public Entity CreateEntity<T>(IntVector2 position) where T : Entity, new() => this.EntitySystem.CreateEntity<T>(position);
	public void DestroyEntity(Entity entity) => this.EntitySystem.DestroyEntity(entity);
	public bool HasEntityAt(Entity entity, IntVector2 position) => this.EntitySystem.HasEntityAt(entity, position);
	public ListView<Entity> GetEntities(IntVector2 position) => this.EntitySystem.GetEntitiesAt(position);
	public void MoveEntity(Entity entity, IntVector2 position) => this.EntitySystem.Move(entity, position);
	public void PutEntity(Entity entity, IntVector2 position) => this.EntitySystem.Put(entity, position);

	public Slice GetSlice(IntVector2 position) {
		return new Slice( 
			position,
			GetGroundCell(position), 
			GetSurfaceCell(position), 
			GetRoofCell(position), 
			GetEntities(position));
	}
}
