﻿using System.Collections.Generic;
using Utilities.Collections;
using Utilities.General;

public struct Slice {
	public readonly IntVector2 Position;
	public readonly Cell GroundCell;
	public readonly Cell SurfaceCell;
	public readonly Cell RoofCell;
	public readonly ListView<Entity> Entities; 
	
	public Slice(IntVector2 position, Cell ground, Cell surface, Cell roof, ListView<Entity> entities) {
		this.Position = position;
		this.GroundCell = ground;
		this.SurfaceCell = surface;
		this.RoofCell = roof;
		this.Entities = entities;
	}

	public Slice(IntVector2 position, Cell ground, Cell surface, Cell roof, List<Entity> entities) : 
		this(position, ground, surface, roof, new ListView<Entity>(entities)) { }
}

