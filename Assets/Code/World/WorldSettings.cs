﻿using System;

public class WorldSettings {
    static float[] TickSpeeds { get; } = { 0.1f, 0.2f, 0.5f, 1f, 2f, 5f, 10f };

    int TickSpeedIndex { get; set; } = 3;

    public float TickSpeed => TickSpeeds[this.TickSpeedIndex];
    
    public void IncrementTickSpeed() {
        this.TickSpeedIndex = Math.Min(this.TickSpeedIndex + 1, TickSpeeds.Length - 1);
    }
    
    public void DecrementTickSpeed() {
        this.TickSpeedIndex = Math.Max(this.TickSpeedIndex - 1, 0);
    }
} 

