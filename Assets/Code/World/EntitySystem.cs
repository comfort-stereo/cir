﻿using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Collections;
using Utilities.Extensions;
using Utilities.General;

public class EntitySystem {
    
	List<Entity> Entities { get; } = new List<Entity>();
	Dictionary<IntVector2, List<Entity>> EntityMap { get; } = new Dictionary<IntVector2, List<Entity>>();
	ListPool<Entity> EntityListPool { get; } = new ListPool<Entity>();

	public void Tick() {
		foreach (var entity in this.Entities) {
			entity.Draw();
			entity.Tick();
		}
	}

	public Entity CreateEntity<T>(IntVector2 position) where T : Entity, new() {	
		var entity = new T(); 
		this.Entities.Add(entity);
		Put(entity, position);
		entity.Initialize(position);
		return entity;
	}

	public void DestroyEntity(Entity entity) {
		var index = this.Entities.IndexOf(entity);
		if (index >= 0) {
            Take(entity);
            this.Entities.UnorderedRemoveAt(index);
		}
		entity.Destroy();
	}
	
	public bool HasEntityAt(Entity entity, IntVector2 position) {
		var list = this.EntityMap.GetOrNull(position);
		if (list == null) {
			return false;
		}
		return list.Contains(entity);
	}

	public ListView<Entity> GetEntitiesAt(IntVector2 position) {
		var list = this.EntityMap.GetOrNull(position);
		if (list == null) {
			return ListView<Entity>.Empty;
		}
		return new ListView<Entity>(list);
	}
	
	public void Move(Entity entity, IntVector2 position) {
		Take(entity);
		Put(entity, position);
	}
	
	public void Put(Entity entity, IntVector2 position) {
		GetOrCreateEntityListAt(position).AddUnique(entity);
		entity.ManagedPosition = position;
	}

	public bool Take(Entity entity) {
		var list = this.EntityMap.GetOrNull(entity.ManagedPosition);
		if (list == null) {
			return false;
		}
		var removed = list.Remove(entity);
		if (list.Count == 0) {
			this.EntityListPool.Return(list);
			this.EntityMap.Remove(entity.ManagedPosition);
		}

		return removed;
	}
	
	List<Entity> GetOrCreateEntityListAt(IntVector2 position) {
		var list = this.EntityMap.GetOrNull(position);
		if (list == null) {
			list = this.EntityListPool.Get();
			this.EntityMap[position] = list;
		}
		return list;
	}
}
