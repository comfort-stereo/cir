﻿using System.Collections.Generic;
using UnityEngine;
using Utilities.General;
using Utilities.Pathfinding;

public delegate bool MovePredicate(IntVector2 position, Direction direction);

public class PathfindingSystem : PathfindingSolver<IntVector2, Direction> {

    const int DefaultMaxNodes = 8000;

    protected List<Direction> DirectionBuffer { get; } = new List<Direction>();

    MovePredicate MovePredicate { get; set; }
    
    public PathfindingSystem() : base(DefaultMaxNodes) { }
    
    public IntVector2 Navigate(IntVector2 start, IntVector2 end, List<Direction> path, MovePredicate movePredicate) {
        this.MovePredicate = movePredicate;
        
        Debug.Log("Regenerating path...");
        IntVector2 nearest;
        
        Find(start, end, path, out nearest);
        
        if (path.Count == 0) {
            Debug.Log("Position unreachable, navigating to nearest node...");
            Find(start, nearest, path, out nearest);
            if (path.Count == 0) {
                Debug.LogError("No possible path was found.");
            } else {
                return nearest;
            }
        }
        
        return end;
    }
    
    protected override float Heuristic(IntVector2 a, IntVector2 b) {
        return Vector2.Distance(a, b);
    }

    protected override float CostForAction(IntVector2 position, Direction direction) {
        return direction.IsDiagonal() ? Constant.Sqrt2 : 1f;
    }

    protected override IntVector2 ApplyAction(IntVector2 position, Direction direction) {
        return position + direction.ToIntVector();
    }

    protected override List<Direction> Expand(IntVector2 position) {
        this.DirectionBuffer.Clear();
        foreach (var direction in DirectionExtensions.All) {
            if (this.MovePredicate(position, direction)) {
                this.DirectionBuffer.Add(direction);
            } 
        }
        return this.DirectionBuffer;
    }
}
