﻿using System.Collections.Generic;
using Application;
using Utilities;
using Utilities.Extensions;
using Utilities.General;

public class Pathfinder {
    
    List<Direction> Path { get; } = new List<Direction>();
    
    MovePredicate MovePredicate { get; }
    
    IntVector2 EndPosition { get; set; }
    
    public bool HasPath => this.Path.Count != 0;
    public Direction Peek => this.Path[this.Path.Count - 1];

    public Pathfinder(MovePredicate predicate) {
        this.MovePredicate = predicate;
    }

    public void Navigate(IntVector2 start, IntVector2 end) {
        this.EndPosition = Global.PathfindingSystem.Navigate(start, end, this.Path, this.MovePredicate);
//        Debug.Log("Pathfinding Node Count: " + Global.PathfindingSystem.NodeCount);
//        Debug.Log("Current Path Length: " + this.Path.Count);
    }
    
    bool ValidatePath(IntVector2 start) {
        for (var i = this.Path.Count - 1; i >= 0; i--) {
            var direction = this.Path[i];
            if (this.MovePredicate(start, direction)) {
                start += direction.ToIntVector();
            } else {
                return false;
            }
        }
        return true;
    }
    
    void UpdatePath(IntVector2 current) {
        if (ValidatePath(current)) {
            return;
        }
        Navigate(current, this.EndPosition);
    }
    
    public Direction Next(IntVector2 current) {
        UpdatePath(current);
        return this.Path.Pop();
    }
}
