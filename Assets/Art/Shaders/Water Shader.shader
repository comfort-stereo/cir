﻿Shader "Unlit/WorldSpaceNormals"
{
    Properties
    {
        _Skew ("Skew", float) = 1
    }
    // no Properties block this time!
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vertex
            #pragma fragment fragment
            // include file that contains UnityObjectToWorldNormal helper function
            #include "UnityCG.cginc"

            struct v2f {
                // we'll output world space normal as one of regular ("texcoord") interpolators
                half3 worldNormal : TEXCOORD0;
                float4 position : SV_POSITION;
            };

            // vertex shader: takes object space normal as input too
            v2f vertex (float4 vertex : POSITION, float3 normal : NORMAL)
            {
                v2f output;
                output.position = UnityObjectToClipPos(vertex);
                // UnityCG.cginc file contains function to transform
                // normal from object to world space, use that
                output.worldNormal = UnityObjectToWorldNormal(normal);
                return output;
            }
            
            fixed4 fragment (v2f i) : SV_Target
            {
                fixed4 color = 0;
                // normal is a 3D vector with xyz components; in -1..1
                // range. To display it as color, bring the range into 0..1
                // and put into red, green, blue components
                color.rgb = i.worldNormal*0.5+0.5-0.5;
                return color;
            }
            ENDCG
        }
    }
}
